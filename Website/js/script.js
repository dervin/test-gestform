function getRandomBetween(min, max) {
 	return Math.floor(Math.random()*(max-min+1)+min);
}

function gestForm(){
	//Taille de la liste aléatoire entre 1 et 5
	var lenghtArray = getRandomBetween(1, 5);
	console.log("Taille de la liste = "+lenghtArray);

	//Création de la liste
	let numbers = [];

	//Peuplement de la liste avec des nombre aléatoires entre -1000 et 1000
	for (var j = 0; j < lenghtArray; j++)
	{
		numbers.push(getRandomBetween(-1000, 1000));
	}

	var ListGestForm = "";

	//Vérification des conditions GestForm
	numbers.forEach(function(item, index, array) {
		console.log("Test des conditions :");
		if(item % 3 == 0 && item % 5 == 0)
	  	{
	  		ListGestForm += "<li>"+item+" : GestForm</li>";
	  		console.log("GestForm -> "+item)
	  	}
		else if (item % 3 == 0)
	  	{
	  		ListGestForm += "<li>"+item+" : Geste</li>";   
	  		console.log("Geste -> "+item);
	  	} 
	  	else if(item % 5 == 0)
	  	{
	  		ListGestForm += "<li>"+item+" : Forme</li>";
	    	console.log("Forme -> "+item);
	  	}
	  	else
	  	{
	  		ListGestForm += "<li>"+item+"</li>";
	  		console.log(item);
	  	}
	});

	//Annonce de la liste
	var spanAnnoucement = document.getElementById("span_ListAnnouncement");
	spanAnnoucement.innerHTML = "Voici une liste de "+lenghtArray+" nombres entiers compris entre -1000 et 1000 : <ul>"+ ListGestForm+"</ul>";
}



